---
title: 'Frequently Asked Questions'
date: 2022-05-03T18:42:00-05:00
draft: false
weight: 1
summary: Entry point.
---
= Frequently Asked Questions

The most common questions related to contributing to Red Hat-based kernel projects.

:sectnums:

{{< toc >}}

== Owners Questions

=== What is the Ownersfile?

https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/blob/main/info/owners.yaml[owners.yaml] is a collection of data which maps which Red Hat SSTs (Sub System Team) are responsible for kernel technologies (aka subsystems) that Red Hat supports.  The owners.yaml file includes other data such as Kernel technology maintainers and reviewers that is useful for the https://gitlab.com/cki-project/kernel-webhooks/[kernel-webhooks] and other utilities.

=== Is there a more human-readable format for the owners.yaml file?

Please view the link:maintainers-list.html[Maintainers List].

== Submission Related Questions

=== I submitted an MR but it was rejected for 'non-technical' issues.  Why did that happen and what does it mean?

CentOS Stream, and by extension Red Hat, welcome all community requests and contributions to the CentOS Stream kernel.  Red Hat applies Development, QE, Documentation, and other resources to every change made to the CentOS Stream kernel to ensure the CentOS Stream and RHEL kernels are secure, highly available, and meet Red Hat's users’ requirements.  The maintainers may reject a change because they are aware of a lack of resources available to provide support for the change.  For example, a community user or SIG may request that a particular driver be enabled and Red Hat may not have access to hardware associated with the driver.  In this case, CentOS Stream and Red Hat cannot provide any guarantee on functionality to users, and as such the request would be rejected.  Please take a moment to understand: We do not like to reject contributions and consider any rejections to be a loss for the overall community behind CentOS Stream and RHEL, but unfortunately, we are also resource constrained.

=== I can support the hardware or software that was rejected for 'non-technical' issues.  Isn't that enough?

Unfortunately, no.  While we appreciate your enthusiasm and help, Red Hat has support requirements it must meet and a lack of ability to reproduce a problem will cause a delay in support for users and customers.

=== Are there any other options to getting my changes into the Centos Stream kernel?

Yes!  You can consider forming a Centos Stream SIG for your changes, or joining a similarly aligned existing SIG.

== Formatting Related Questions
:sectnums:
=== anchor:badformatting[] The formatting is bad, how do I fix it?
:sectnums:
.. Descriptions and comments utilize https://docs.gitlab.com/ee/user/markdown.html[GitLab Flavored Markdown] to handle the formatting of the text.
.. Consecutive lines separated by single newlines, without a blank line in between, will be treated as a paragraph for purposes of display. The MR tools will still see the newlines and will treat such labeled fields (“JIRA:”, etc) as intended. If you want a cleaner display, insert a blank line between entries.
.. Indented blocks of text get a box around them, which can cause unintended consequences on the formatting of the display of your description..

== Label Related Questions
:sectnums:
=== I have a red `Unmet Requirement` label image:images/qsg-label_image-red_blank.png["Blank Red Label"], what does this mean and how do I fix it?
:sectnums:
.. Review the listed Dependencies in the MR Summary.
.. Find each of the MR associated with the Dependencies line(s)
.. Compare the common commits between this MR and each of the dependent MR commits.
... If any of the common commits have *different* ID's, the Dependency check will be marked as RED.
... To resolve, this MR must be rebased upon the tree containing the mismatched commit ID.
=== I opened a new MR, but there is a image:images/qsg-label_image_cki_missing.png["Red CKI:Missing"] Label.  What is the problem?
:sectnums:
.. First, confirm whether you can see a Pipelines tab on the MR itself.
... Try to REMOVE the image:images/qsg-label_image_cki_missing.png["Red CKI:Missing"] label.  This will trigger the system to re-run the webhook and will likely return as image:images/qsg-label_image_cki_ok.png["Blue CKI:OK"] if the test ran OK but the webhook hasn't posted the update yet.
... If the image:images/qsg-label_image_cki_missing.png["Red CKI:Missing"] label returns, go into the Pipelines tab and press the image:images/qsg-run_pipeline.png["Run Pipeline"] button.  That will generate some results that should satisfy the unmet requirement.
.. If the Pipelines tab is missing:
... A possible cause is that the source branch (on your fork) is named `main`.  The name `main` is protected and is not able to trigger a pipeline job.  To resolve this, close the current MR, rename the branch on your fork and open a new MR.
... Another possible cause is that you have insufficient permissions.  Find another associate in your organization that can look to confirm whether they see the missing tab (and image:images/qsg-run_pipeline.png[“Run Pipeline”] button on that tab)
.... If they can see it, ask them to click the image:images/qsg-run_pipeline.png["Run Pipeline"] button and then refer to <<weirdmr,My MR is behaving weirdly, I'm not sure what to do...>>

=== What does image:images/qsg-label_image_targetedtestingmissing.png["TargetedTestingMissing label"] indicate?
:sectnums:
.. This is currently only an informational alert.  It does not have an impact on the ability of an MR to be merged.
... https://cki-project.org/docs/background/gitlab-pipelines/mr-full/#blocking-on-missing-targeted-testing[This is intended to transition to a blocking tag in the future], but there are no timelines attached to this effort currently.
.. It is currently consumed by QA teams that are working on identifying testing gaps.  The label indicates that the changed code in this commit did not have any specific tests that target the affected area.  Baseline testing (booting, functional, standard regression, etc.) is still performed and validated.

=== What does image:images/qsg-label_image_externalci_needstesting.svg["ExternalCI::NeedsTesting label"] indicate?
:sectnums:
.. This label indicates that the Merge Request must pass a CI that is not part of the usual Continuous Kernel Integration (CKI). Such external CIs are managed by individual subsystems. Look for other ExternalCI labels (such as image:images/qsg-label_image_lnst_needstesting.svg["ExternalCI::lnst::NeedsTesting label"]; see below) to find out what subsystem or subsystems mandated the testing.

=== What does image:images/qsg-label_image_lnst_needstesting.svg["ExternalCI::lnst::NeedsTesting label"] indicate?
:sectnums:
.. This label indicates that the Merge Request modifies kernel networking and requires network specific testing by the https://github.com/LNST-project/lnst[LNST test suite]. Usually, you don't have to do anything. Your merge request will be automatically picked up and tested. If the tests pass, the label will be changed to image:images/qsg-label_image_lnst_ok.svg["ExternalCI::lnst::OK label"].
.. If your merge request previously modified kernel networking by mistake and you later changed that and force pushed the MR, the image:images/qsg-label_image_lnst_needstesting.svg["ExternalCI::lnst::NeedsTesting"] label will not be automatically removed. In such case, you can remove the label yourself. It is safe to attempt the removal; if the MR still modifies the kernel networking, the label will be restored. Note that kernel networking is not limited to image:images/qsg-label_image_subsystem_net.svg["Subsystem::net"].
.. If your merge request is critical and you need to expedite the testing, contact the Networking Team kernel maintainers at mailto:kernel-net-submaint@redhat.com[kernel-net-submaint@redhat.com].

== MR Interactions
:sectnums:
=== How do I make the webhook re-evaluate the readiness of a MR?
:sectnums:
.. Remove the label you want re-checked by clicking the 'x' in the Labels area as defined above under step 8.  (i.e. if you have image:images/qsg-label_image_jira_needsreview.png["Red Jira::NeedsReview Label"], remove that label from the MR)

=== The link I received to create the MR for my change only refers to my fork and not the upstream kernel tree. Why did this happen and how do I resolve it?
:sectnums:
.. This typically happens if you have inadvertently cloned your fork and the `origin` remote points at your fork rather than the upstream tree.
.. It is recommended that you clone the upstream tree, not your fork. +
If that is not an option, then you can specify the remote to use when creating the MR. +
_(https://groups.google.com/a/redhat.com/g/kernel-info/c/B0cUCO8IoIY/m/F0_GGO0zAQAJ[reported on kernel-info].)_

=== anchor:commonformattingissues[] I followed the commit rules as detailed, why do I have a red label that is seemingly satisfied? +
:sectnums:
Things to be aware of:
... Even extra leading whitespace can cause problems, and may not be obvious in some browsers.
.... https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9/-/merge_requests/141[https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9/-/merge_requests/141] shows an example where fields were indented but not rendered as such.
... Beware that some formatting is masked by the web interface that can only be seen via the edit interface. (for example, the <>'s around an email address in the DCO sign-off)
.... https://gitlab.com/redhat/rhel/src/kernel/rhel-8/-/merge_requests/1720[https://gitlab.com/redhat/rhel/src/kernel/rhel-8/-/merge_requests/1720] shows an example of this.

=== anchor:weirdmr[] My MR is behaving weirdly, I'm not sure what to do...
:sectnums:
.. First and foremost, make sure to click the https://red.ht/GitLabSSO[GitLabSSO link] to refresh your permissions.
.. Reach out to your KWF Representative for assistance.
+
image::images/qsg-KWF_Representatives_2022Q2.png["KWF Reps"]

.. Send an email to mailto:kernel-info@redhat.com[kernel-info@redhat.com].
.. If there's a GitLab specific issue, you can open an issue at https://gitlab.com/redhat/rhel/src/kernel/bugreports[Red Hat Kernel bug reports].

=== How can I determine the status of any related Jiras on my MR?
:sectnums:
.. The jira webhook will post a comment to the MR with the current status of each referenced ticket.  Each time the webhook is run, that comment will be edited to reflect the current state.

=== Can I edit my comments?
:sectnums:
.. Minor modifications to an existing MR comment are allowed.
.. A minor modiviation can be defined mostly as correcting a typo or adjusting the language of a sentence that is otherwise unclear (i.e. perhaps there's a word missing).
.. *It is important to keep comments as close to their original intent as possible since they are used for auditing purposes.*

=== anchor:getartifactsdirectly[] How do I get build artifacts without looking at Jira? +
:sectnums:
.. Search by Merge Request ID.
+
image::images/qsg-faq-get-mr-id.png["Merge Request ID"]
.. Navigate to the https://datawarehouse.cki-project.org/[CKI Datawarehouse] tab and search for the `Merge Request ID`.
+
image::images/qsg-faq-datawarehouse-search-for-builds.png["Search for Builds"]

**It is possible to search by a pipeline ID.**

.. Find the pipelines tab image:images/qsg-faq-pipelines.png["Pipelines Tab"] and click it
.. Find the most recent build and click the pipeline number +
+
image::images/qsg-faq-pipelines_jobs.png["Pipeline Jobs Links"]

=== When I attempt to click the new Merge Request link after a push, I get an error.  How do I resolve it? +
:sectnums:

Occasionally, when clicking the link that is provided after a `git push`, the browser page that opens returns an HTTP 500 error.

```
   500
   Whoops, something went wrong on our end.
   Request ID: 01GQ0QHBS01AZNMPGYFC0JBEG9

   Try refreshing the page, or going back and attempting the action again.

   Please contact your GitLab administrator if this problem persists.
```

This is caused by an issue on the GitLab server side.  Try the following steps to resolve or work around the problem:

. Retry after a few minutes. (Try this step a couple of times before moving on)
.. Sometimes it can just be a transient problem and waiting 5-10 minutes may be enough to fix the problem.
. Consider opening a https://gitlab.com/redhat/rhel/gitlab#user-content-service-issues[support ticket] with GitLab.
. Work around the problem by using xref:lab.adoc[the lab utility] or https://gitlab.com/gitlab-org/cli[gitlab-cli].

=== Why is GitLab asking for credit card information in my MR?

Pipelines are trying to run in your personal namespace. This can have various reasons:

- You have submitted a merge request against your own fork instead of the gitlab.com/redhat namespace. You can resolve this by resubmitting the merge request against the correct target project.
- You have used a source branch name that is protected *in the target project*, for example, 'main'.  You can resolve this by resubmitting your merge request from a source branch that does not match any of the protected branches of the target project.
