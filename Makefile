MAKEFLAGS := --no-print-directory

.PHONY: all clean docs-prepare docs-clean docs selftest
all:
	scripts/checks.sh

info/RHMAINTAINERS: info/owners.yaml templates/RHMAINTAINERS.template
	python3 scripts/owners-tool.py convert info/owners.yaml templates/RHMAINTAINERS.template > info/RHMAINTAINERS

clean: docs-clean

themes/hugo-geekdoc/theme.toml:
	mkdir -p themes/hugo-geekdoc
	cd themes/hugo-geekdoc; \
	wget -q -O - https://github.com/thegeeklab/hugo-geekdoc/releases/download/v0.38.1/hugo-geekdoc.tar.gz | tar -xz

content/docs/owners_yaml_format.adoc: templates/owners-schema.yaml
	python3 scripts/owners-tool.py doc $< > $@

content/docs/maintainers-list.adoc: info/owners.yaml templates/maintainers-list.template
	python3 scripts/owners-tool.py convert $+ > $@

docs-prepare: themes/hugo-geekdoc/theme.toml content/docs/owners_yaml_format.adoc \
	      content/docs/maintainers-list.adoc

docs-clean:
	rm -rf resources themes
	rm -f content/docs/owners_yaml_format.adoc

docs: docs-prepare
	hugo server

selftest:
	@echo "!!! This target runs selftests of internal tools. It does NOT test owners.yaml changes."
	@echo "!!! If you want to test owners.yaml, run 'make' without parameters."
	$(MAKE) -C selftest
