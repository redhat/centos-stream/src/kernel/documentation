**CentOS Stream Kernel Documentation**

See the [full CentOS Stream and RHEL kernel documentation](https://redhat.gitlab.io/centos-stream/src/kernel/documentation/).

See https://red.ht/kernel_workflow_doc for information on the Red Hat Kernel
Workflow.

This repository contains the source code of the documentation and the
onwers.yaml file.

**Subsystem Owners**

The file `info/owners.yaml` is the canonical source of all information that
must be mapped to kernel areas of responsibility.  As the name of the file
indicates, the file is in YAML format.  It is easy to read for both humans
and machines and many languages (python, go, etc.) include YAML parsers.

**Code Approvals, Maintainers and Reviewers**

The owners.yaml file has two categories of code approvers: maintainers, and
reviewers.  Maintainers are engineers who are considered the owners of the
subsystem and are responsible for it.  Reviewers are engineers who are
trusted by the maintainers to approve code changes in the subsystem.  The
[kernel-workflow](https://gitlab.com/cki-project/kernel-workflow/) creates
approval rules based on the content of owners.yaml.  Both maintainers and
reviewers can approve code changes for the given subsystem.

**Making Changes**

Changes to the documentation and to owners.yaml must be made through a Merge
Request to the [Red Hat kernel documentation project](https://gitlab.com/redhat/centos-stream/src/kernel/documentation).
Changes must be accompanied by a description of the modifications.  In most
cases, a simple explanation will do (for example, "Update x86 maintainers").

Users making changes must include a "Signed-off-by:" tag on all commits that
acknowledges the DCO, https://developercertificate.org.

Upon submission, your changes will be verified by the documentation project
CI.  If you want to check your changes locally before submission, run `make`
at the top level of the documentation project.

Tip: the easiest way to do changes to owners.yaml is using the GitLab Edit
File feature.  Simply browse to info/owners.yaml file from the main project
page, click the Edit button and select "Edit single file".  Do not forget to
add your Signed-off-by to the commit message.

**RHMAINTAINERS**

Previously, we used to generate RHMAINTAINERS file from owners.yaml.  That
file had the same format as MAINTAINERS in the kernel tree.  Some people
prefer this format over yaml.  It is still possible to build that file;
just invoke:

```
make info/RHMAINTAINERS
```

**owners.yaml Merge Request Approval Rules**

1.  Included and excluded file changes can be merged if the MR author is a subsystem maintainer.
If the author is not a subsystem maintainer, then the subsystem maintainer must provide an approve.

2.  Any MR adding an engineer in a role must be authored by or approved by the added engineer.
An additional approve from a subsystem maintainer is required, unless the maintainer is the author
of the MR.

3.  Any MR removing an engineer in a role must be authored by or approved by the removed engineer,
except in the case when the removed engineer is no longer with Red Hat.  While removals from roles
do not require the approve of the maintainer, MR authors are encouraged to add the maintainer for
an approve.

4.  Any MR adding or modifying other fields (devel-sst, status, jira, etc.) requires the approval
from the subsystem maintainer.

MR authors, reviewers, and maintainers should discuss disagreements about ownership or role changes
with their management.

**owners-schema.yaml Changes**

If you are changing SST names, you must ensure the SST names themselves, and
the SST name changes in the file are approved by RHEL management. If you are
changing the format of owners.yaml, you must ensure the changes have been
approved by the KWF (Kernel Workflow) group. Changes that have failed to
meet this will be removed by reverting commits.

**allowed-hosts**

The file `info/allowed-hosts` is a list of \*redhat.com domains that are either
public or are explicitly approved by Red Hat Information Security to expose
in public repositories.  This file is consumed by CI pipelines across various
kernel test projects to ensure that Red Hat internal hostnames are not made
publicly visible.

**Project Layout**

The layout is

- content/docs/ contains the general kernel workflow documentation, links, etc.
- scripts/ contains scripts for generating documentation, etc.
- info/ contains the latest information (owners.yaml, etc.)

**Project Maintainers**

The documentation project is maintained by:

- Derek Barbosa (@debarbos)
- Jiri Benc (@jbencrh)
